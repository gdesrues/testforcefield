import Sofa

class Scene(Sofa.PythonScriptController):
    def __init__(self, node):
        self.createGraph(node)

    def createGraph(self, rootNode):
        # rootNode
        self.rootNode = rootNode
        rootNode.dt = 0.01
        rootNode.gravity = "0 0 0"
        rootNode.createObject('VisualStyle', displayFlags='showVisual showWireframe showForceFields showBehavior')
        rootNode.createObject('DefaultAnimationLoop')
        rootNode.createObject('DefaultVisualManagerLoop')
        rootNode.createObject('RequiredPlugin', pluginName='Flexible')
        rootNode.createObject('EulerImplicitSolver', rayleighStiffness=0.1, rayleighMass=0.1)
        rootNode.createObject('CGLinearSolver', iterations=100)


        # rootNode/Bar
        Bar = rootNode.createChild('Bar')
        self.Bar = Bar
        Bar.createObject('MeshVTKLoader', name='loader', filename="bar.vtk")
        Bar.createObject('Mesh', name="mesh", src="@loader")

        Bar.createObject('MechanicalObject', name='dof', template="Affine", showObject=1, showObjectScale=0.1)
        Bar.createObject("BarycentricShapeFunction")

        Bar.createObject('BoxROI', box='0 0 0 0 1 1', position='@dof.position', template="Affine", drawSize=10)
        Bar.createObject('FixedConstraint', indices="@[-1].indices")



        # Bar/behavior
        behavior = Bar.createChild('behavior')
        self.behavior = behavior
        behavior.createObject('TopologyGaussPointSampler', inPosition='@../mesh.position', method=2, order=1)
        behavior.createObject('MechanicalObject', template='F331')
        behavior.createObject('MLSMapping', template='Affine,F331', showDeformationGradientScale=0.15)


        # rootNode/Bar/behavior/E
        E = behavior.createChild('E')
        self.E = E
        E.createObject('MechanicalObject', name='E', template='E331')
        E.createObject('GreenStrainMapping', template='F331,E331')
        E.createObject('HookeForceField', template='E331', poissonRatio=0.4, youngModulus=15e3)


        # rootNode/Bar/collision
        collison = Bar.createChild("collison")
        self.collison = collison
        collison.createObject('Mesh', src="@../loader")
        collison.createObject("MechanicalObject", template="Vec3d")
        collison.createObject("MLSMapping", template="Affine,Vec3d")


        # rootNode/Bar/visual
        visual = Bar.createChild('visual')
        self.visual = visual
        visual.createObject('MeshSTLLoader', name='stlloader', filename="bar.stl")
        visual.createObject('OglModel', name='Visual', template='ExtVec3f', src='@stlloader', collisonor='gray')
        visual.createObject('MLSMapping', template='Affine,ExtVec3f')



def createScene(rootNode):
    myScene = Scene(rootNode)
    return 0
